local seeded_shop_gold = RegisterMod("Seeded Donation Machine", 1);

function seeded_shop_gold:spawnFoolsGold(_)
    if ((Game():GetRoom():GetType() == RoomType.ROOM_SHOP) and -- Is Shop
        (Game():GetSeeds():IsCustomRun()) and -- Is a Seeded Run @TODO Fix Challenge Runs
        (Game():GetRoom():IsFirstVisit()) and -- First Visit
        (Game():GetRoom():GetBossID() == 0) -- No Boss
        ) then

        -- return if there's already a donation machine
        if (not seeded_shop_gold:isSlotMachinePresent()) then
            return nil -- returning anything else breaks things
        end

        -- offset to shopkeep. one bomb two stone
        Isaac.GridSpawn(GridEntityType.GRID_ROCK_GOLD, 0, Game():GetRoom():GetTopLeftPos() + Vector(150, 0))
        Isaac.GridSpawn(GridEntityType.GRID_ROCK_GOLD, 0, Game():GetRoom():GetTopLeftPos() + Vector(175, 0))
        Isaac.GridSpawn(GridEntityType.GRID_ROCK_GOLD, 0, Game():GetRoom():GetTopLeftPos() + Vector(225, 0))
    end
end

-- returns bool if Slot Machine - Donation Machine variant is present. 
-- in seeded runs the donation machine is apparently still present 
function seeded_shop_gold:isSlotMachinePresent()
    local roomEntities = Game():GetRoom():GetEntities()
        for i = 0, #roomEntities - 1, 1 do
            local entity = roomEntities:Get(i)
            if (entity.Type == EntityType.ENTITY_SLOT and entity.Variant == 8) then
                return true
            end
        end
    return false
end

seeded_shop_gold:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, seeded_shop_gold.spawnFoolsGold)